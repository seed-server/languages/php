
####################################
developpement
####################################

des outils pour gérer le projet


framgit.org
===================================

* code source https://framagit.org/GwikGwik/visites3d
* wiki https://framagit.org/GwikGwik/visites3d/-/wikis/home


documenter sur framagit
---------------------------------------------

* [les tickets](https://framagit.org/GwikGwik/visites3d/-/issues) gerer les fonctionalités et les erreurs
* [le wiki](https://framagit.org/GwikGwik/visites3d/-/wikis/home) créer une doc pour clarifier le fonctionnement


synchronyser le code
-------------------------------------------

comment utiliser framagit sur une machine :
le tuto a bougé [ici](https://framagit.org/GwikGwik/visites3d/-/wikis/mac-git-utilisation)



nextcloud
===================================

* discussion https://nx.anadenn.fr/call/fxtb6um8

