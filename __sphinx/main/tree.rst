
##################################
arbres de programmation
##################################

faire un lego.

les éléments s'emboitent et s'appellent les un les autres.
le client (js) et le serveur (php) se partagent les actions.

* atomiser les procédures et les recombiner
* décrire les execution dans un arbre pour en appeler certaines parties
* évite la redondance de code
* permet un débuggage rapide des fonctions
* ajouter des fonctionalité sans toucher à la structure

js et php partagent la même architecture de base :

* l'objet Tree est codé dans les deux langages
* un certain nombre d'objets existent côté client et serveur
* le développement est centré sur les points forts de chaque langage
* peu de code dans les plugins

php
===================

* centré sur la structure
* peu de html
* gestion des données


js
=====================

* affichage
* interactions utilisateurs
* appèle le serveur pour les taches complexes
