#################################
Le programme
#################################

.. toctree::
   :glob:
   :maxdepth: 3

   architecture
   main
   tree
   plugins

