
######################################
structure du projet
######################################


src : les codes
=================================


essentiellement des classes

* les codes javascript
* les codes php

js
-------------------------------------

les classes javascript permettant de structurer
le document avec des fichiers xml

php
-------------------------------------

les classes php documentées plus bas

parts
-------------------------------------

les fichiers php à inclure pour créer le document :

* head.php
* footer.php
* headerbar.php

docs
-------------------------------------

les fichiers sources de cette documentation

static : les fichiers externes
=================================

* bibliothèques java script
* css
* images par défaut


plugins : les plugins
=================================

une liste de plugins qui s'ajoutent dans le programme

docs
=================================

la doc générée par PhpDocumentor

projects
=================================

les projets créés
