<?php
include("./settings.php");

$application=get_app($MAIN_PATH."/main.xml");


//--------------------------------------------
function get_node($request){
//--------------------------------------------
    global $application;

    if(!array_key_exists("path",$request)){
        error_404();
    }

    if(!array_key_exists("action",$request)){
        error_404();
    }

    $node=$application->find($request["path"]);

    if($node==null){
        error_404();
    }

    return $node;

}
//--------------------------------------------

if($_SERVER['REQUEST_METHOD'] == 'POST') {
        $node=get_node($_POST);
        $action=$_POST["action"];
        $request=$_POST;
        //var_dump($_POST);
}else{
        $node=get_node($_GET);
        $action=$_GET["action"];
        $request=$_GET;
}

switch($action){
    case "page":

        include($PART_DIRECTORY."/head.php");
        include($PART_DIRECTORY."/headerbar.php");
        $node->get_page($_GET);
        include($PART_DIRECTORY."/footer.php");
        break;
    case "view":
        $node->call($request);
    case "call":
        if( array_key_exists("selected",$request) ){
            $selected=$application->find($request["selected"]);
            $full_page=false;
            if ($node instanceof Action){
                $full_page=true;
            }
          if ( array_key_exists("full_page",$request)){
              if ( $request["full_page"]=="1"){
                    $full_page=true;
                }
              else if ( $request["full_page"]=="0"){
                    $full_page=false;
                }
            }


            if ($full_page==true){
                include($PART_DIRECTORY."/head.php");
                include($PART_DIRECTORY."/headerbar.php");
            }

                $node->selected=$selected;
                $node->call($request);

            if ($full_page==true){
                include($PART_DIRECTORY."/footer.php");
           }
        }
        break;
    case "api":
        $node->get_api($_POST);
        break;
    default:
        echo "nothing";
} 

?>
